package testCases;

import io.qameta.allure.*;
import jdk.jfr.Description;
import org.junit.jupiter.api.*;
import pageObject.LoginPage;
import pageObject.MainPage;
import sesion.Session;

public class TodoistTest {
    private MainPage mainPage = new MainPage();
    private LoginPage loginPage = new LoginPage();

    @BeforeEach
    public void before() {
        Session.getInstance().getDriver().get("https://todoist.com/");
    }

    @DisplayName("Ingreso fallido al sistema")
    @Description("Verifica que el sistema muestra el error cuando se introducen credenciales de acceso incorrectas.")
    @Severity(SeverityLevel.CRITICAL)
    @Test
    public void testFailedLogin() {
        mainPage.loginButton.click();
        loginPage.email.setValue("example@example.com");
        loginPage.password.setValue("12345");
        loginPage.loginButton.click();
        Assertions.assertTrue(loginPage.errorMessageLabel.isDisplayed());
    }

    @AfterEach
    public void after() {
        Session.getInstance().close();
    }
}
