package factoryBrowser;

public class FactoryBrowser {
    public static IBrowser make(String browserType) {
        IBrowser browser;
        switch (browserType) {
            case "proxy":
                browser = new ChromeProxy();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + browserType);
        }
        return browser;
    }
}
