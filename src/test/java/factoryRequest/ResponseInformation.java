package factoryRequest;

public class ResponseInformation {
    private String body;
    private int code;

    public String getBody() {
        return body;
    }

    public int getCode() {
        return code;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
