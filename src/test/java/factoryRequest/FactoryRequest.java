package factoryRequest;

public class FactoryRequest {


    public static Request make (String type){
        Request request;

        switch (type){
            default:
                request = new RequestGET();
                break;
        }

        return request;

    }
}
