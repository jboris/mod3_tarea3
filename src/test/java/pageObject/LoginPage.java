package pageObject;

import controlSelenium.Button;
import controlSelenium.Label;
import controlSelenium.TextBox;
import org.openqa.selenium.By;

public class LoginPage {
    public TextBox email = new TextBox(By.id("email"));
    public TextBox password = new TextBox(By.id("password"));
    public Button loginButton = new Button(By.xpath("//*[@id=\"login_form\"]/button"));
    public Label errorMessageLabel = new Label(By.xpath("/html/body/main/div/div[2]/div[1]/div[2]/div/span"));
}
