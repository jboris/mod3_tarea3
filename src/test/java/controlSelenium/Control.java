package controlSelenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import sesion.Session;

public class Control {
    protected By locator;
    protected WebElement control;

    public Control(By locator) {
        this.locator = locator;
    }

    protected void find() {
        this.control = Session.getInstance().getDriver().findElement(locator);
    }

    public void click() {
        this.find();
        this.control.click();
    }

    public void setValue(String value) {
        this.find();
        this.control.clear();
        this.control.sendKeys(value);
    }

    public boolean isDisplayed() {
        try {
            this.find();
            return this.control.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }
}
